const groceriesData = [
    {
        name: "Strawberry",
        key: "strawberry"
    },
    {
        name: "Blueberry",
        key: "blueberry"
    },
    {
        name: "Orange",
        key: "orange"
    },
    {
        name: "Banana",
        key: "banana"
    },
    {
        name: "Apple",
        key: "apple"
    },
    {
        name: "Carrot",
        key: "carrot"
    },
    {
        name: "Celery",
        key: "celery"
    },
    {
        name: "Mushroom",
        key: "mushroom"
    },
    {
        name: "Green Pepper",
        key: "greenPepper"
    },
    {
        name: "Eggs",
        key: "eggs"
    },
    {
        name: "Cheese",
        key: "cheese"
    },
    {
        name: "Butter",
        key: "butter"
    },
    {
        name: "Chicken",
        key: "chicken"
    },
    {
        name: "Beef",
        key: "beef"
    },
    {
        name: "Pork",
        key: "pork"
    },
    {
        name: "Fish",
        key: "fish"
    },
    {
        name: "Rice",
        key: "rice"
    },
    {
        name: "Pasta",
        key: "pasta"
    },
    {
        name: "Bread",
        key: "bread"
    }
]

export default groceriesData;