import React from "react";
import groceriesData from "../groceriesData";
function Groceries() {
  return (
    <>
      <h3 className="heading">
        <i className="fa fa-leaf" aria-hidden="true"></i>Groceries
      </h3>
      <ul className="groceriesList">
        {groceriesData.length > 0
          ? groceriesData.map((item) => {
              return (
                <li key={item.key}>
                  <span>
                    <i className="fa fa-plus-square" aria-hidden="true"></i>
                  </span>
                  {item.name}
                </li>
              );
            })
          : ""}
      </ul>
    </>
  );
}

export default Groceries;
