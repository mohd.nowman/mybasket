import React from 'react'

function Header() {
    return (
        <>
            <i className="fa fa-shopping-basket fa-6" aria-hidden="true"></i>
            <h1 className="App-title">Hello, Basket!</h1>
        </>
    )
}

export default Header