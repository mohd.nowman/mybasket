import React from 'react'
import Basket from './Basket'
import Groceries from './Groceries'

function GroceriesBasketContainer() {
    return (
        <div className='GroceriesBasketContainer'>
            <div className='left'>
                <Groceries />
            </div>
            <div className='right'>
                <Basket />
            </div>

        </div>
    )
}

export default GroceriesBasketContainer