import React from "react";
import basketData from "../basketData";

function Basket() {
  return (
    <>
      <div className="heading">
        <h3>
          <i className="fa fa-shopping-basket" aria-hidden="true"></i>
          Basket
        </h3>
        <h3 className="clearBasket">
          <i className="fa fa-trash-o" aria-hidden="true"></i>
        </h3>
      </div>
      <ul className="basketItemsList">
        {basketData.length > 0
          ? basketData.map((item) => {
              return (
                <li key={item.key}>
                  <span>
                    <i className="fa fa-minus-square" aria-hidden="true"></i>
                  </span>
                  {item.quantity} {item.name}
                </li>
              );
            })
          : ""}
      </ul>
    </>
  );
}

export default Basket;
