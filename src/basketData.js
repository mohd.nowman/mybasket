const basketData = [
    {
        name: "Strawberry",
        key: "strawberry",
        quantity: 1
    },
    {
        name: "Carrot",
        key: "carrot",
        quantity: 1
    },

    {
        name: "Eggs",
        key: "eggs",
        quantity: 1
    },

    {
        name: "Chicken",
        key: "chicken",
        quantity: 1
    },

    {
        name: "Rice",
        key: "rice",
        quantity: 1
    }
]

export default basketData;