import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import { useState } from 'react';
import GroceriesBasketContainer from './components/GroceriesBasketContainer';

function App() {
  const [search, setSearch] = useState("");

  const handleSearchGroceries = (e) => {
    const searchInput = e.target.value;
    setSearch(e.target.value);
    console.log("searchInput: ", searchInput);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Header />
      </header>

      <nav>
        <form>
          <input
            type="text"
            placeholder='Search Groceries'
            value={search}
            onChange={handleSearchGroceries}
          />
        </form>
      </nav>

      <main><GroceriesBasketContainer /></main>

      <footer className='App-footer'>
        <Footer />
      </footer>
    </div>
  );
}

export default App;
